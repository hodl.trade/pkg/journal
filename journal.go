package journal

import (
	"io"
	"time"
)

type Journal interface {
	Next(time.Time) bool
	Rotator
}

func Daily(rotator Rotator) Journal {
	return New(24*time.Hour, rotator)
}

func New(interval time.Duration, rotator Rotator) Journal {
	return &journal{interval, rotator, time.Time{}}
}

type journal struct {
	interval time.Duration
	r        Rotator

	// state
	n time.Time
}

func (t *journal) Next(ts time.Time) bool {
	return ts.After(t.n) || t.n.IsZero()
}

func (t *journal) Rotate(ts time.Time) (io.WriteCloser, error) {
	t.n = ts.Truncate(t.interval).Add(t.interval - time.Nanosecond)
	return t.r.Rotate(ts)
}

type Rotator interface {
	Rotate(time.Time) (io.WriteCloser, error)
}

type RotatorFn func(ts time.Time) (w io.WriteCloser, err error)

func (fn RotatorFn) Rotate(ts time.Time) (io.WriteCloser, error) {
	return fn(ts)
}
